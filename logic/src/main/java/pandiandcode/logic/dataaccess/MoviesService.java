package pandiandcode.logic.dataaccess;

import pandiandcode.logic.model.Response.ConfigurationResponse;
import pandiandcode.logic.model.Response.GenresResponse;
import pandiandcode.logic.model.Response.MoviesResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public interface MoviesService {

    public static final String VERSION = "3";
    public static final String MOVIE = "/movie";
    public static final String POPULAR = "/popular";
    public static final String SEARCH = "/search";
    public static final String GENRE = "/genre";
    public static final String LIST = "/list";
    public static final String CONFIGURATION = "/configuration";

    public static final String PAGE = "page";
    public static final String QUERY = "query";
    public static final String INCLUDE_ADULTS = "include_adults";

    public static final String POPULAR_MOVIE_PATH = VERSION + MOVIE + POPULAR;
    public static final String SEARCH_MOVIE_PATH = VERSION + SEARCH + MOVIE;
    public static final String GENRE_MOVIES_PATH = VERSION + GENRE + MOVIE + LIST;
    public static final String CONFIGURATION_PATH = VERSION + CONFIGURATION;

    @GET(MoviesService.POPULAR_MOVIE_PATH)
    public Call<MoviesResponse> getPopularMovies(@Query(PAGE) int page);

    @GET(MoviesService.SEARCH_MOVIE_PATH)
    public Call<MoviesResponse> searchMovies(@Query(PAGE) int page,
                                             @Query(QUERY) String query,
                                             @Query(INCLUDE_ADULTS) boolean includeAdults);

    @GET(MoviesService.GENRE_MOVIES_PATH)
    public Call<GenresResponse> getGenres();

    @GET(MoviesService.CONFIGURATION_PATH)
    public Call<ConfigurationResponse> getConfiguration();
}
