package pandiandcode.logic.dataaccess.repository;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import pandiandcode.logic.dataaccess.MoviesService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class BaseRepository<T> {

    private static final String API_URL = "https://api.themoviedb.org/";

    protected static final String API_KEY = "93aea0c77bc168d8bbce3918cefefa45";
    protected static final String LANGUAGE = "en-US";

    public static final String API_KEY_KEY = "api_key";
    public static final String LANGUAGE_KEY = "language";

    protected MoviesService mServiceProxy;

    protected Map<String, List<Call<T>>> callMap = new HashMap<>();

    public BaseRepository() {

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        HttpUrl url = request.url().newBuilder()
                                .addQueryParameter(API_KEY_KEY, API_KEY)
                                .addQueryParameter(LANGUAGE_KEY, LANGUAGE)
                                .build();
                        request = request.newBuilder().url(url).build();
                        return chain.proceed(request);
                    }
                })
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mServiceProxy = retrofit.create(MoviesService.class);

    }

    protected void executeCall(final DataRepositoryCallback<T> callback, Call<T> call, final String tag) {

        if (callMap.containsKey(tag)) {
            cancelCalls(callMap.get(tag));
            callMap.remove(tag);
        }
        addCallToMap(tag, call);
        call.enqueue(new Callback<T>() {
            @Override public void onResponse(Call<T> call, Response<T> response) {
                if (response != null && !call.isCanceled()) {
                    Log.d(BaseRepository.class.getSimpleName(), response.toString());
                    callback.onResponse(response.body());
                }
                removeCallFromMap(tag, call);
            }

            @Override public void onFailure(Call<T> call, Throwable t) {
                if (!call.isCanceled()) {
                    t.printStackTrace();
                    callback.onError(new Exception(t));
                }
                removeCallFromMap(tag, call);
            }
        });

    }

    private void cancelCalls(List<Call<T>> calls) {
        if (calls != null && !calls.isEmpty()) {
            for (Call<T> call : calls) {
                call.cancel();
            }
        }
    }

    private void addCallToMap(String tag, Call<T> call) {
        List<Call<T>> calls = callMap.get(tag);
        if (calls == null) {
            calls = new ArrayList<>();
        }
        calls.add(call);
        callMap.put(tag, calls);
    }

    private void removeCallFromMap(String tag, Call<T> call) {
        List<Call<T>> calls = callMap.get(tag);
        if (calls != null) {
            calls.remove(call);
        }
        callMap.put(tag, calls);
    }

    public interface DataRepositoryCallback<T> {
        void onResponse(T response);

        void onError(Exception error);
    }

}
