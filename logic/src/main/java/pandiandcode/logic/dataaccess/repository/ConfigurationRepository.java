package pandiandcode.logic.dataaccess.repository;

import android.util.Log;

import pandiandcode.logic.model.Response.ConfigurationResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */
public class ConfigurationRepository extends BaseRepository<ConfigurationResponse> {

    private static ConfigurationRepository instance;

    public synchronized static ConfigurationRepository getInstance() {
        if (instance == null) {
            instance = new ConfigurationRepository();
        }

        return instance;
    }

    private ConfigurationResponse configurationResponse;

    public void loadConfiguration(final DataRepositoryCallback<ConfigurationResponse> callback) {
        if (configurationResponse == null) {
            Call<ConfigurationResponse> call = mServiceProxy.getConfiguration();
            executeCall(callback, call);
        }
    }

    public ConfigurationResponse getConfiguration() {
        return configurationResponse;
    }

    protected void executeCall(final DataRepositoryCallback<ConfigurationResponse> callback, Call<ConfigurationResponse> call) {
        call.enqueue(new Callback<ConfigurationResponse>() {
            @Override public void onResponse(Call<ConfigurationResponse> call, Response<ConfigurationResponse> response) {
                if (response != null) {
                    Log.d(BaseRepository.class.getSimpleName(), response.toString());
                    configurationResponse = response.body();
                    callback.onResponse(response.body());
                }

            }

            @Override public void onFailure(Call<ConfigurationResponse> call, Throwable t) {
                callback.onError(new Exception(t));
            }
        });
    }

}
