package pandiandcode.logic.dataaccess.repository;

import pandiandcode.logic.model.Response.GenresResponse;
import retrofit2.Call;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class GenreRepository extends BaseRepository<GenresResponse> {

    public void getGenres(final DataRepositoryCallback<GenresResponse> callback) {
        Call<GenresResponse> call = mServiceProxy.getGenres();
        executeCall(callback, call, "genres");
    }

}
