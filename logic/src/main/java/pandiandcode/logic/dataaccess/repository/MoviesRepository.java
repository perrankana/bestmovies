package pandiandcode.logic.dataaccess.repository;

import pandiandcode.logic.model.Response.MoviesResponse;
import retrofit2.Call;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class MoviesRepository extends BaseRepository<MoviesResponse> {

    public void getPopularMovies(int page, final DataRepositoryCallback<MoviesResponse> callback) {
        Call<MoviesResponse> call = mServiceProxy.getPopularMovies(page);
        executeCall(callback, call, "popular_movies_" + page);
    }

    public void searchMovies(int page, String query, boolean isAdults, final DataRepositoryCallback<MoviesResponse>
            callback) {
        Call<MoviesResponse> call = mServiceProxy.searchMovies(page, query, isAdults);
        executeCall(callback, call, "search_movies_" + page);
    }

}
