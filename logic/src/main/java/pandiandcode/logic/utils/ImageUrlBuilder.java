package pandiandcode.logic.utils;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class ImageUrlBuilder {

    private String baseUrl;

    private String size;

    private String filePath;

    public ImageUrlBuilder baseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public ImageUrlBuilder size(String size) {
        this.size = size;
        return this;
    }

    public ImageUrlBuilder filePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    public String build() {
        return baseUrl + size + filePath;
    }

}
