package pandiandcode.logic.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Rocio Ortega on 26/02/2017.
 */

public class DateUtils {

    private final static String FORMAT = "yyyy-MM-dd";

    private final static String FORMAT_YEAR = "yyyy";

    public static Date getDate(String dateString) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT);
        return simpleDateFormat.parse(dateString);
    }

    public static String getYear(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT_YEAR);
        String time = "";
        try {
            time = simpleDateFormat.format(getDate(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

}
