package pandiandcode.logic.inject;

import android.app.Application;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class LogicInjector {
    private static LogicInjector instance;

    public static LogicInjector getInstance() {
        if (instance == null) {
            instance = new LogicInjector();
        }

        return instance;
    }

    LogicComponent logicComponent;

    public void init(Application application) {
        logicComponent = DaggerLogicComponent.builder()
                .appModule(new AppModule(application))
                .repositoryModule(new RepositoryModule()).build();
    }

    public LogicComponent getLogicComponent() {
        return logicComponent;
    }
}
