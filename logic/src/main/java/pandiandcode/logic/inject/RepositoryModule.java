package pandiandcode.logic.inject;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pandiandcode.logic.dataaccess.repository.ConfigurationRepository;
import pandiandcode.logic.dataaccess.repository.GenreRepository;
import pandiandcode.logic.dataaccess.repository.MoviesRepository;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */
@Module
public class RepositoryModule {

    @Provides
    MoviesRepository provideMoviesRepository() {
        return new MoviesRepository();
    }

    @Provides
    GenreRepository provideGenresRepository() {
        return new GenreRepository();
    }

    @Provides
    @Singleton
    ConfigurationRepository provideConfigurationRepository() {
        return ConfigurationRepository.getInstance();
    }
}
