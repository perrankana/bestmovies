package pandiandcode.logic.inject;

import javax.inject.Singleton;

import dagger.Component;
import pandiandcode.logic.model.Movie;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */
@Singleton
@Component(modules = {AppModule.class, RepositoryModule.class})
public interface LogicComponent {
    void inject(Movie movie);
}
