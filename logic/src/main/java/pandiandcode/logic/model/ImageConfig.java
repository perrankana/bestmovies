package pandiandcode.logic.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class ImageConfig {

    @SerializedName("base_url")
    private String baseUrl;

    @SerializedName("secure_base_url")
    private String secureBaseUrl;

    @SerializedName("backdrop_sizes")
    private ArrayList<String> backdropSizes;

    @SerializedName("logo_sizes")
    private ArrayList<String> logoSizes;

    @SerializedName("poster_sizes")
    private ArrayList<String> posterSizes;

    @SerializedName("profile_sizes")
    private ArrayList<String> profileSizes;

    @SerializedName("still_sizes")
    private ArrayList<String> stillSizes;

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getSecureBaseUrl() {
        return secureBaseUrl;
    }

    public ArrayList<String> getBackdropSizes() {
        return backdropSizes;
    }

    public ArrayList<String> getLogoSizes() {
        return logoSizes;
    }

    public ArrayList<String> getPosterSizes() {
        return posterSizes;
    }

    public ArrayList<String> getProfileSizes() {
        return profileSizes;
    }

    public ArrayList<String> getStillSizes() {
        return stillSizes;
    }
}
