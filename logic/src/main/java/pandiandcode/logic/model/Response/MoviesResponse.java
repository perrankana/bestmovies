package pandiandcode.logic.model.Response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import pandiandcode.logic.model.Movie;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public final class MoviesResponse {

    private int page;

    @SerializedName("total_results")
    private int totalResults;

    @SerializedName("total_pages")
    private int totalPages;

    private ArrayList<Movie> results;

    public MoviesResponse() {
    }

    public MoviesResponse(int page, int totalResults, int totalPages, ArrayList<Movie> results) {
        this.page = page;
        this.totalResults = totalResults;
        this.totalPages = totalPages;
        this.results = results;
    }

    public int getPage() {
        return page;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public ArrayList<Movie> getResults() {
        return results;
    }
}
