package pandiandcode.logic.model.Response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import pandiandcode.logic.model.ImageConfig;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public final class ConfigurationResponse {

    private ImageConfig images;

    @SerializedName("change_keys")
    private ArrayList<String> changeKeys;

    public ImageConfig getImages() {
        return images;
    }

    public ArrayList<String> getChangeKeys() {
        return changeKeys;
    }
}
