package pandiandcode.logic.model.Response;

import java.util.ArrayList;

import pandiandcode.logic.model.Genre;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public final class GenresResponse {

    private ArrayList<Genre> genres;

    public ArrayList<Genre> getGenres() {
        return genres;
    }

    public GenresResponse() {
    }

    public GenresResponse(ArrayList<Genre> genres) {
        this.genres = genres;
    }
}
