package pandiandcode.logic.model;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by Rocio Ortega on 26/02/2017.
 */
public class DataError {

    public static final int DEFAULT_ERROR = 0;
    public static final int NO_RESULTS = 1;

    @Retention(SOURCE)
    @IntDef({DEFAULT_ERROR, NO_RESULTS})
    public @interface ErrorType {
    }

    private @DataError.ErrorType int errorType;

    private RetryCallback callback;

    public DataError() {
    }

    public DataError(@DataError.ErrorType int errorType, RetryCallback callback) {
        this.errorType = errorType;
        this.callback = callback;
    }

    public @DataError.ErrorType int getErrorType() {
        return errorType;
    }

    public boolean shouldRetry() {
        return errorType == DEFAULT_ERROR;
    }

    public void onRetryClicked() {
        if (callback != null) {
            callback.onRetryClicked();
        }
    }

    public interface RetryCallback {
        void onRetryClicked();
    }
}
