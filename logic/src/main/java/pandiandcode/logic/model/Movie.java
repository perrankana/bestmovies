package pandiandcode.logic.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.inject.Inject;

import pandiandcode.logic.dataaccess.repository.ConfigurationRepository;
import pandiandcode.logic.inject.LogicInjector;
import pandiandcode.logic.utils.DateUtils;
import pandiandcode.logic.utils.ImageUrlBuilder;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class Movie {

    @Inject
    ConfigurationRepository configuration;

    @SerializedName("poster_path")
    private String posterPath;

    private boolean adult;

    private String overview;

    @SerializedName("release_date")
    private String releaseDate;

    @SerializedName("genre_ids")
    private ArrayList<Integer> genreIds;

    private long id;

    @SerializedName("original_title")
    private String originalTitle;

    @SerializedName("original_language")
    private String originalLanguage;

    private String title;

    @SerializedName("backdrop_path")
    private String backdropPath;

    private double popularity;

    @SerializedName("vote_count")
    private int voteCount;

    private boolean video;

    @SerializedName("vote_average")
    private double voteAverage;

    public Movie() {
        LogicInjector.getInstance().getLogicComponent().inject(this);
    }

    public Movie(String posterPath, boolean adult, String overview, String releaseDate, ArrayList<Integer> genreIds, long id, String originalTitle, String originalLanguage, String title, String backdropPath, double popularity, int voteCount, boolean video, double voteAverage) {
        this();
        this.posterPath = posterPath;
        this.adult = adult;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.genreIds = genreIds;
        this.id = id;
        this.originalTitle = originalTitle;
        this.originalLanguage = originalLanguage;
        this.title = title;
        this.backdropPath = backdropPath;
        this.popularity = popularity;
        this.voteCount = voteCount;
        this.video = video;
        this.voteAverage = voteAverage;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public boolean isAdult() {
        return adult;
    }

    public String getOverview() {
        return overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public ArrayList<Integer> getGenreIds() {
        return genreIds;
    }

    public long getId() {
        return id;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public String getTitle() {
        return title;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public double isPopularity() {
        return popularity;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public boolean isVideo() {
        return video;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    public String getReleaseYear() {
        return DateUtils.getYear(releaseDate);
    }

    public String getBackdropUrl() {
        if (configuration.getConfiguration() != null && configuration.getConfiguration().getImages() != null) {
            ImageConfig imageConfig = configuration.getConfiguration().getImages();
            return new ImageUrlBuilder()
                    .baseUrl(imageConfig.getBaseUrl())
                    .size(imageConfig.getBackdropSizes().get(0))
                    .filePath(backdropPath)
                    .build();
        }
        return "";
    }
}


