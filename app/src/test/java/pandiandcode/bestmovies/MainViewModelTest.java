package pandiandcode.bestmovies;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;

import pandiandcode.logic.dataaccess.repository.BaseRepository;
import pandiandcode.logic.dataaccess.repository.MoviesRepository;
import pandiandcode.logic.model.Movie;
import pandiandcode.logic.model.Response.MoviesResponse;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */
@RunWith(RobolectricTestRunner.class)
public class MainViewModelTest {

    @Mock
    private MoviesRepository mockApi;

    @Captor
    private ArgumentCaptor<BaseRepository.DataRepositoryCallback<MoviesResponse>> cb;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);


    }

    @Test
    public void shouldReceiveMoviesFromApi() throws Exception {
        Mockito.verify(mockApi).getPopularMovies(Mockito.anyInt(), cb.capture());

        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(new Movie());
        movies.add(new Movie());
        movies.add(new Movie());

        MoviesResponse moviesResponse = new MoviesResponse(1, movies.size(), 1, movies);

        cb.getValue().onResponse(moviesResponse);

        // TODO: 25/02/2017 Make assertions

    }
}
