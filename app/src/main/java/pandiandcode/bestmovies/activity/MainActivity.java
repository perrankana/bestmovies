package pandiandcode.bestmovies.activity;

import android.app.SearchManager;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import pandiandcode.bestmovies.R;
import pandiandcode.bestmovies.databinding.ActivityMainBinding;
import pandiandcode.bestmovies.viewmodel.MainViewModel;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class MainActivity extends BindingBaseActivity<MainViewModel> {

    private MainViewModel viewModel;

    @Override protected MainViewModel getViewModel() {
        if (viewModel == null) {
            viewModel = new MainViewModel(this);
        }
        return viewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setViewModel(getViewModel());
        setupToolbar(binding.toolbar);
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        setupSearchView(searchView);
        return super.onCreateOptionsMenu(menu);
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.popular:
                getViewModel().showPopularMovies();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private void setupSearchView(SearchView searchView) {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(getString(R.string.search));
        searchView.setOnQueryTextListener(getViewModel().getSearchListener());
    }

}
