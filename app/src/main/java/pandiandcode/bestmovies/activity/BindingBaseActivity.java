package pandiandcode.bestmovies.activity;

import android.support.v7.app.AppCompatActivity;

import pandiandcode.bestmovies.viewmodel.BaseViewModel;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public abstract class BindingBaseActivity<T extends BaseViewModel> extends AppCompatActivity {

    protected abstract T getViewModel();

}
