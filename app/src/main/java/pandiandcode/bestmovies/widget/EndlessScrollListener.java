package pandiandcode.bestmovies.widget;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener {

    private static final int VISIBLE_THRESHOLD = 6;
    private static final int STARTING_PAGE_INDEX = 1;

    private int previousTotalItemCount = 0;
    private int currentPage = 1;

    private boolean isLoading = true;

    @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();

        int totalItemCount = layoutManager.getItemCount();
        int lastVisibleItemPosition = getLastVisibleItemPosition(layoutManager);

        if (lastVisibleItemPosition >= 0) {

            if (totalItemCount < previousTotalItemCount) {
                currentPage = STARTING_PAGE_INDEX;
                previousTotalItemCount = totalItemCount;
                if (totalItemCount == 0) {
                    isLoading = true;
                }
            }

            if (isLoading && (totalItemCount > previousTotalItemCount)) {
                isLoading = false;
                previousTotalItemCount = totalItemCount;
            }

            if (!isLoading
                    && (lastVisibleItemPosition + VISIBLE_THRESHOLD) > totalItemCount
                    && (currentPage != 1 || (lastVisibleItemPosition + 1 != totalItemCount))) {
                currentPage++;
                onLoadMore(currentPage, totalItemCount, recyclerView);
                isLoading = true;
            }
        }

    }

    private int getLastVisibleItemPosition(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager != null) {
            if (layoutManager instanceof LinearLayoutManager) {
                return ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
            }
        }
        return -1;
    }

    public abstract void onLoadMore(int page, int totalItemsCount, RecyclerView view);
}
