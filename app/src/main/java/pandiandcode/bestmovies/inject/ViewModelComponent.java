package pandiandcode.bestmovies.inject;

import javax.inject.Singleton;

import dagger.Component;
import pandiandcode.bestmovies.viewmodel.MainViewModel;
import pandiandcode.logic.inject.AppModule;
import pandiandcode.logic.inject.RepositoryModule;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */
@Singleton
@Component(modules = {AppModule.class, RepositoryModule.class})
public interface ViewModelComponent {
    void inject(MainViewModel mainViewModel);
}
