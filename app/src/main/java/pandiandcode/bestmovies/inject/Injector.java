package pandiandcode.bestmovies.inject;

import android.app.Application;

import pandiandcode.logic.inject.AppModule;
import pandiandcode.logic.inject.RepositoryModule;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class Injector {

    private static Injector instance;

    public static Injector getInstance() {
        if (instance == null) {
            instance = new Injector();
        }

        return instance;
    }

    ViewModelComponent viewModelComponent;

    public void init(Application application) {
        viewModelComponent = DaggerViewModelComponent.builder()
                .appModule(new AppModule(application))
                .repositoryModule(new RepositoryModule()).build();
    }

    public ViewModelComponent getViewModelComponent() {
        return viewModelComponent;
    }
}
