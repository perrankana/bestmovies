package pandiandcode.bestmovies.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pandiandcode.bestmovies.BR;
import pandiandcode.bestmovies.R;
import pandiandcode.logic.model.DataError;
import pandiandcode.logic.model.Genre;
import pandiandcode.logic.model.Movie;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class MoviesAdapter extends RecyclerView.Adapter<BindingHolder> {

    private static final int ITEM_TYPE = 0;
    private static final int LOADING_TYPE = 1;
    private static final int ERROR_TYPE = 2;

    private List<Movie> movies;
    private List<Genre> genres;

    private boolean showError;
    private int page;
    private int totalPages;

    private DataError dataError;

    public MoviesAdapter(List<Movie> movies, List<Genre> genres, int page, int totalPages, DataError dataError) {
        this.movies = movies;
        this.genres = genres;
        this.page = page;
        this.totalPages = totalPages;
        showError(dataError);
    }

    public void update(List<Movie> movies) {
        showError = false;
        int totalCountBefore = getItemCount();
        this.movies.addAll(movies);
        notifyItemRangeInserted(totalCountBefore, movies.size());
    }

    public void showError(DataError dataError) {
        if (dataError != null) {
            showError = true;
            this.dataError = dataError;
            notifyItemChanged(getItemCount() - 1);
        }
    }

    @Override public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE:
                return new BindingHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_movie_item, parent, false));
            case LOADING_TYPE:
                return new BindingHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_loading_item, parent, false), false);
            case ERROR_TYPE:
                return new BindingHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_error_item, parent, false));
        }
        return null;
    }

    @Override public void onBindViewHolder(BindingHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case ITEM_TYPE:
                Movie movie = movies.get(position);

                holder.getBinding().setVariable(BR.movie, movie);
                holder.getBinding().setVariable(BR.genres, genres);

                holder.getBinding().executePendingBindings();

                break;
            case LOADING_TYPE:
                break;
            case ERROR_TYPE:
                holder.getBinding().setVariable(BR.dataError, dataError);

                holder.getBinding().executePendingBindings();

                setViewClickEvent(holder.getBinding().getRoot());

                break;
        }
    }

    @Override public int getItemCount() {
        return (movies == null ? 0 : movies.size()) + additionalItems();
    }

    @Override public int getItemViewType(int position) {
        if (position == movies.size()) {
            if (showError) {
                return ERROR_TYPE;
            } else {
                return LOADING_TYPE;
            }
        }
        return ITEM_TYPE;
    }

    private int additionalItems() {
        return page == totalPages && !showError ? 0 : 1;
    }

    private void setViewClickEvent(View rootView) {
        View retryButton = rootView.findViewById(R.id.retry_button);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                showError = false;
                notifyItemChanged(getItemCount() - 1);
                dataError.onRetryClicked();
            }
        });
    }

}
