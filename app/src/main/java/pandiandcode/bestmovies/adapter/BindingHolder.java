package pandiandcode.bestmovies.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class BindingHolder extends RecyclerView.ViewHolder {

    private ViewDataBinding binding;

    public BindingHolder(View rowView) {
        this(rowView, true);
    }

    public BindingHolder(View rowView, boolean canBind) {
        super(rowView);
        if (canBind) {
            binding = DataBindingUtil.bind(rowView);
        }
    }

    public ViewDataBinding getBinding() {
        return binding;
    }

}
