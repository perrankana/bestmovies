package pandiandcode.bestmovies.bindinghelper;

import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import pandiandcode.logic.model.Genre;

/**
 * Created by Rocio Ortega on 26/02/2017.
 */

public class GenresBindingAdapter {

    @BindingAdapter(value = {"genres", "genresIds"}, requireAll = false)
    public static void bindGenres(TextView view, ArrayList<Genre> genres, ArrayList<Integer> genresIds) {
        String genreList = "";
        if (genresIds != null && !genresIds.isEmpty()) {
            for (int genreId : genresIds) {
                String genre = findGenre(genres, genreId);
                if (!TextUtils.isEmpty(genre)) {
                    genreList += (TextUtils.isEmpty(genreList) ? "" : ", ") + genre;
                }
            }
        }
        if (TextUtils.isEmpty(genreList)) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
            view.setText(genreList);
        }
    }

    private static String findGenre(ArrayList<Genre> genres, int genreId) {
        if (genres != null && !genres.isEmpty()) {
            for (Genre genre : genres) {
                if (genre.getId() == genreId) {
                    return genre.getName();
                }
            }
        }
        return "";
    }
}
