package pandiandcode.bestmovies.bindinghelper;

import android.databinding.BindingAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import pandiandcode.bestmovies.R;
import pandiandcode.logic.model.DataError;

/**
 * Created by Rocio Ortega on 26/02/2017.
 */

public class ErrorBindingAdapter {

    @BindingAdapter(value = {"errorType"}, requireAll = false)
    public static void setErrorIcon(ImageView view, @DataError.ErrorType int errorType) {

        switch (errorType) {
            case DataError.NO_RESULTS:
                view.setImageResource(R.drawable.ic_no_results);
                break;
            case DataError.DEFAULT_ERROR:
            default:
                view.setImageResource(R.drawable.ic_no_connection);
                break;
        }
    }

    @BindingAdapter(value = {"errorType"}, requireAll = false)
    public static void setErrorText(TextView view, @DataError.ErrorType int errorType) {

        switch (errorType) {
            case DataError.NO_RESULTS:
                view.setText(R.string.no_results_error);
                break;
            case DataError.DEFAULT_ERROR:
            default:
                view.setText(R.string.default_error);
                break;
        }
    }
}
