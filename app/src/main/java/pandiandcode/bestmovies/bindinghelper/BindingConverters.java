package pandiandcode.bestmovies.bindinghelper;

import android.databinding.BindingConversion;
import android.view.View;

/**
 * Created by Rocio Ortega on 26/02/2017.
 */

public class BindingConverters {

    @BindingConversion
    public static int convertBooleanToVisibility(boolean visible) {
        return visible ? View.VISIBLE : View.GONE;
    }
}
