package pandiandcode.bestmovies.bindinghelper;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import pandiandcode.bestmovies.adapter.MoviesAdapter;
import pandiandcode.bestmovies.widget.EndlessScrollListener;
import pandiandcode.logic.model.DataError;
import pandiandcode.logic.model.Genre;
import pandiandcode.logic.model.Movie;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class MoviesAdapterBinder {

    @BindingAdapter({"movies", "genres", "page", "totalPages", "endlessScrollListener", "dataError"})
    public static void bind(RecyclerView recyclerView, List<Movie> movies, List<Genre> genres, int page, int totalPages,
                            EndlessScrollListener endlessScrollListener, DataError dataError) {

        if (page == 1) {
            MoviesAdapter adapter = new MoviesAdapter(movies, genres, page, totalPages, dataError);
            recyclerView.setAdapter(adapter);
            recyclerView.addOnScrollListener(endlessScrollListener);
        } else if (recyclerView.getAdapter() != null && recyclerView.getAdapter() instanceof MoviesAdapter) {
            if (dataError != null) {
                ((MoviesAdapter) recyclerView.getAdapter()).showError(dataError);
            } else {
                ((MoviesAdapter) recyclerView.getAdapter()).update(movies);
            }
        }
    }

}
