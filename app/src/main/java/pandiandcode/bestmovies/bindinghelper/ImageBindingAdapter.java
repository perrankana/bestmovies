package pandiandcode.bestmovies.bindinghelper;

import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import pandiandcode.bestmovies.R;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class ImageBindingAdapter {

    @BindingAdapter(value = {"android:src"}, requireAll = false)
    public static void setImageUrl(ImageView view, String url) {
        if (!TextUtils.isEmpty(url)) {
            Picasso.with(view.getContext())
                    .load(url)
                    .placeholder(R.drawable.ic_placeholder)
                    .fit()
                    .into(view);
        } else {
            view.setImageResource(R.drawable.ic_placeholder);
        }
    }
}
