package pandiandcode.bestmovies.viewmodel;

import android.content.Context;
import android.databinding.Bindable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;

import java.util.List;

import javax.inject.Inject;

import pandiandcode.bestmovies.BR;
import pandiandcode.bestmovies.inject.Injector;
import pandiandcode.bestmovies.widget.EndlessScrollListener;
import pandiandcode.logic.dataaccess.repository.BaseRepository;
import pandiandcode.logic.dataaccess.repository.ConfigurationRepository;
import pandiandcode.logic.dataaccess.repository.GenreRepository;
import pandiandcode.logic.dataaccess.repository.MoviesRepository;
import pandiandcode.logic.model.DataError;
import pandiandcode.logic.model.Genre;
import pandiandcode.logic.model.Movie;
import pandiandcode.logic.model.Response.ConfigurationResponse;
import pandiandcode.logic.model.Response.GenresResponse;
import pandiandcode.logic.model.Response.MoviesResponse;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class MainViewModel extends BaseViewModel {

    @Inject
    MoviesRepository moviesRepository;

    @Inject
    GenreRepository genreRepository;

    @Inject
    ConfigurationRepository configurationRepository;

    private List<Movie> movies;

    private List<Genre> genres;

    private DataError dataError;

    private int page;

    private int totalPages;

    private String query;

    private Handler searchHandler = new Handler();
    private final Runnable searchRunnable = new Runnable() {
        @Override public void run() {
            search();
        }
    };

    private EndlessScrollListener endlessScrollListener = new EndlessScrollListener() {
        @Override public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
            MainViewModel.this.page = page;
            loadMoreMovies();
        }
    };

    private SearchView.OnQueryTextListener searchListener = new SearchView.OnQueryTextListener() {
        @Override public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override public boolean onQueryTextChange(final String newText) {
            if (!TextUtils.isEmpty(newText) && newText.length() > 1) {

                query = newText;
                searchHandler.removeCallbacks(searchRunnable);
                searchHandler.postDelayed(searchRunnable, 500);

            }

            return false;
        }
    };

    private DataError.RetryCallback errorCallback = new DataError.RetryCallback() {
        @Override public void onRetryClicked() {
            if (configurationRepository.getConfiguration() == null) {
                loadConfiguration();
            } else {
                loadMoreMovies();
            }
        }
    };

    public MainViewModel(Context context) {
        super(context);
        Injector.getInstance().getViewModelComponent().inject(this);

        loadConfiguration();
    }

    public SearchView.OnQueryTextListener getSearchListener() {
        return searchListener;
    }

    public void showPopularMovies() {
        initPages();
        query = null;
        setDataError(null);
        loadMovies();
    }

    @Bindable
    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
        notifyPropertyChanged(BR.movies);
    }

    @Bindable
    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
        notifyPropertyChanged(BR.genres);
    }

    @Bindable
    public DataError getDataError() {
        return dataError;
    }

    public void setDataError(DataError dataError) {
        this.dataError = dataError;
        notifyPropertyChanged(BR.dataError);
    }

    @Bindable
    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
        notifyPropertyChanged(BR.page);
    }

    @Bindable
    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
        notifyPropertyChanged(BR.totalPages);
    }

    @Bindable
    public EndlessScrollListener getEndlessScrollListener() {
        return endlessScrollListener;
    }

    private void loadData() {
        initPages();
        loadGenres();
        loadMovies();
    }

    private void search() {
        initPages();
        movies.clear();
        setMovies(movies);
        searchMovies();
    }

    private void initPages() {
        totalPages = 1;
        page = 1;
    }

    private void loadMoreMovies() {
        if (TextUtils.isEmpty(query)) {
            loadMovies();
        } else {
            searchMovies();
        }
    }

    private void loadConfiguration() {
        if (configurationRepository.getConfiguration() == null) {
            configurationRepository.loadConfiguration(new BaseRepository.DataRepositoryCallback<ConfigurationResponse>() {
                @Override public void onResponse(ConfigurationResponse response) {
                    loadData();
                }

                @Override public void onError(Exception error) {
                    loadData();
                }
            });
        } else {
            loadData();
        }
    }

    private void loadGenres() {
        genreRepository.getGenres(new BaseRepository.DataRepositoryCallback<GenresResponse>() {
            @Override public void onResponse(GenresResponse response) {
                setGenres(response.getGenres());
            }

            @Override public void onError(Exception error) {

            }
        });
    }

    private void loadMovies() {
        if (page <= totalPages) {
            moviesRepository.getPopularMovies(page, getMoviesCallback());
        }
    }

    private void searchMovies() {
        if (page <= totalPages) {
            moviesRepository.searchMovies(page, query, false, getMoviesCallback());
        }
    }

    @NonNull private BaseRepository.DataRepositoryCallback<MoviesResponse> getMoviesCallback() {
        return new BaseRepository.DataRepositoryCallback<MoviesResponse>() {
            @Override public void onResponse(MoviesResponse response) {
                setTotalPages(response.getTotalPages());
                setPage(response.getPage());
                setMovies(response.getResults());
                if (response.getResults().isEmpty()) {
                    setDataError(new DataError(DataError.NO_RESULTS, errorCallback));
                } else {
                    setDataError(null);
                }
                setDataLoaded(true);
            }

            @Override public void onError(Exception error) {
                setDataLoaded(true);
                setDataError(new DataError(DataError.DEFAULT_ERROR, errorCallback));
            }
        };
    }


}
