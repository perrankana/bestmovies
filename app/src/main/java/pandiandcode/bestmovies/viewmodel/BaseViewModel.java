package pandiandcode.bestmovies.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import pandiandcode.bestmovies.BR;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class BaseViewModel extends BaseObservable {

    private Context context;

    private boolean isDataLoaded;

    public BaseViewModel(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @Bindable
    public boolean isDataLoaded() {
        return isDataLoaded;
    }

    public void setDataLoaded(boolean dataLoaded) {
        isDataLoaded = dataLoaded;
        notifyPropertyChanged(BR.dataLoaded);
    }
}
