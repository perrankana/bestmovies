package pandiandcode.bestmovies;

import android.app.Application;

import pandiandcode.bestmovies.inject.Injector;
import pandiandcode.logic.inject.LogicInjector;

/**
 * Created by Rocio Ortega on 25/02/2017.
 */

public class BestMoviesApplication extends Application {

    @Override public void onCreate() {
        super.onCreate();
        initInjector();
    }

    protected void initInjector() {
        LogicInjector.getInstance().init(this);
        Injector.getInstance().init(this);
    }
}
